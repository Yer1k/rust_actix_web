use actix_web::{get, web, App, HttpServer, HttpResponse, Responder};
use microservice::greedy_coin_change;
use serde_json::json;

// Root Route for Change Machine
#[get("/")]
async fn root() -> impl Responder {
    "
    Greedy Coin Change Machine

    **Primary Route:**
    /change/dollars/cents
    "
}

#[get("/change/{dollars}/{cents}")]
async fn change(info: web::Path<(u32, u32)>) -> impl Responder {
    let (dollars, cents) = info.into_inner();
    // Convert to cents amount
    let amount = dollars * 100 + cents;
    let change = greedy_coin_change(amount);
    let response_body = json!({
        "dollars": dollars,
        "cents": cents,
        "change": change
    });
    HttpResponse::Ok().json(response_body)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(root)
            .service(change)
    })
    .bind("127.0.0.1:3000")?
    .run()
    .await
}
