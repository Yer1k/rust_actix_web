# Rust Actix Web Service

[![CI/CD status](https://gitlab.com/Yer1k/rust_actix_web/badges/main/pipeline.svg)](https://gitlab.com/Yer1k/rust_actix_web/-/commits/main)

## Demo
**Click on the image** to see the demo of the web service running in a Docker container.
[![demo](screenshots/demo.png)](https://youtu.be/QhBHUYq8b2o)


## Description
This is a simple web service written in Rust using the Actix web framework. The service has a single endpoint that returns a JSON response. It is containerized using Docker and testing the deployment with Gitlab CI/CD pipeline.

The demo app is the Greedy Algorithm to find Minimum number of coins to make a given amount. 

Say we have only have 1, 5, 10, 25 cents and we need to give 1 dollar and 3 cents in total to someone. The greedy algorithm will suggest us to hand in 25, 25, 25, 25, 1, 1 and 1 cent coins. This app will return the json response as follows:

```json
{
"cents":3,
"change":[25,25,25,25,1,1,1],
"dollars":1
}
```
![example](./screenshots/example.png)

## Flowchart of the app
![Flowchart](./screenshots/rust_actix_web.png)

## How to run the app
### Running the app locally with docker container
1. Clone the repository
2. Run the following command to start the app
    
    `docker build -t myimage .`
    
    `docker run -dp 3000:3000 myimage #runs in background`
    
    `#find image`
    
    `docker ps`
    
    `#kill image (from id you found)`
    
    `#docker stop 4a496e7eab64`

3. Open your browser and navigate to `http://localhost:3000/` or 'http://http://127.0.0.1:3000/' to see the app running

    Test the app by adding the following query parameters to the URL:
    `/change/1/3`

    The response should be:
    ```json
    {"cents":3,"change":[25,25,25,25,1,1,1],"dollars":1}
    ```

4. Or use curl to test the app

    `curl http://localhost:3000/`



## Screenshot of the app running on the docker container
### Terminal
![Screenshot of container](./screenshots/docker.png)
![Screenshot of running 1](./screenshots/run.png)
![Screenshot of running 2](./screenshots/run_1.png)
### Browser
![Screenshot of running 3](./screenshots/web_run.png)
![Screenshot of running 4](./screenshots/web_run_1.png)